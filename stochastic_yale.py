# Pierpaolo Mingolla
#
# Stochastic Yale format
#
# Written in Python 3.8
# Matrices are in form of a list of lists, events are tied to a generic random variable X with integers between 0 and n as its domain,
# saying "event i occurs" is the same as saying "X=i". 

from random import random,randint,gauss,sample
from math import cos

# Compressing and decompressing

def compress(M):
	nz_values=[] # vector of nonzero values
	col_indices=[] # vector of column indices
	order=len(M) # stochastic matrices are square
	for row in M:
		for col_index in range(order):
			elem=row[col_index]
			if elem<0:
				print("negative value")
				return
			elif elem>1:
				print("value greater than 1")
				return
			elif elem>0:
				nz_values.append(elem)
				col_indices.append(col_index)
	return [nz_values,col_indices]

def decompress(M):
	N=[]
	nnz=len(M[0]) # number of nonzero elements
	s=0 # sum of read elements, when it reaches 1 it means the row is full (except it misses some zeroes)
	for i in range(nnz): # in this iteration the function scans both vectors of the compressed form
		elem=M[0][i] # element curently read
		col=M[1][i] # its column index
		if s==0: # it means elem is the first nonzero element of a row
			N.append([]) # a new empty row is built
			prev_col=-1 # needed for the next iteration
		else:
			prev_col=M[1][i-1] # column index of the previous element
		for j in range(col - prev_col - 1): # 0-padding between two nonzero elements
			N[-1].append(0)
		N[-1].append(elem)
		s+=elem
		if s>1: # something's wrong here
			print("sum exceeds 1",i,s)
			return
		elif s==1: # the function has read all the nonzero elements from a specific row, time to reset the sum and see if there are other values next
			s=0
	return padding(N) # the rows are not completed until they're filled with their last zeroes

def padding(M):
	l=len(M)
	for row in M:
		n=len(row)
		row+=([0]*(l-n))
	return M

# Running a Markov chain with an uncompressed matrix

def next(M,i,p): # Given the matrix M, the event i and a real value p between 0 and 1, returns the next occurring event j
    try:
        cumul=M[i][0]
    except:
        print("matrix order",len(M))
        print("index too high",i)
    j=0
    while p>cumul:
        j+=1
        cumul+=M[i][j]
    return j

def random1(): # random() includes 0 ed excludes 1, we need the contrary
    r=random()
    if r==0:
        return 1
    else:
        return r

def run(M,i,n): # Given a Markov chain with matrix M and event i, it prints the occurred events in n steps
    event=i
    print("Starting event:",event)
    for step in range(n-1):
        event=next(M,event,random1())
        print("Event",event)
    event=next(M,event,random1())
    print("Final event:",event)

# Running a Markov chain with a compressed matrix

def cut(l,i): # returns the starting and ending indeces of the i-th sublist of l, cut in such way that every sublist sums to 1
    if l!=[]:
        s=0
        count=0
        while s<1:
            s+=l[count]
            count+=1
        if s>1:
            print("cut() error: sum exceeds 1")
        else:
            if i==0:
                return 0, count-1
            else:
                a, b = cut(l[count:],i-1)
                return count+a, count+b
    else:
        print(" cut() error: empty list, is the index too high?")

def next_comp(M,i,p): # like next() but for compressed matrices
    e1, e2 = cut(M[0],i)
    N=[M[0][e1:e2+1],M[1][e1:e2+1]] # N is M cut from column e1 to column e2
    cumul=N[0][0]
    j=0
    while p>cumul:
        j+=1
        cumul+=N[0][j]
    return N[1][j]


def run_comp(M,i,n): # like run() but for compressed matrices
    event=i
    print("Starting event:",event)
    for step in range(n-1):
        event=next_comp(M,event,random1())
        print("Event",event)
    event=next_comp(M,event,random1())
    print("Final event:",event)

# Generating a random matrix

def normal(l): # takes a list l of positive values and divides them by their sum
    s=sum(l)
    if s<0:
        print("normal() error: negative sum")
    elif s>0:
        for i in range(len(l)-1):
            l[i]/=s
        l[-1]=1-sum(l[:-1]) # for approximation errors
    return l

def generate(): # generates an uncompressed random matrix
    order=randint(5000,10000)
    M=[[0]*order]*order
    for i in range(order):
        for j in range(order):
            M[i][j]=abs(cos(gauss(0,1))) # random values between 0 and 1
    return [normal(row) for row in M] # rows must follow the sum to 1 rule

def generate_comp(): # generates a compressed random matrix
    nz_values=[]
    col_indices=[]
    order=randint(5000,10000)
    for i in range(order):
        n=randint(1,order) # number of nonzero elements in the i-th row, it must be at least 1
        nz_temp=[] # list of the nonzero values in the i-th row
        count=0
        while count < n: # it generates random elements, WARNING: there's an infinitesimal chance it never ends
            random_value=abs(cos(gauss(0,1)))
            if random_value != 0:
                nz_temp.append(random_value)
                count+=1
        nz_values+=normal(nz_temp)
        col_indices+=sorted(sample(range(order),len(l1_temp)))
    return [nz_values,col_indices]

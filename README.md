Time-homogeneous Markov chains are described by a transition matrix that doesn't change over time. This matrix is a right stochastic matrix, which means every entry is nonnegative and every row sums to 1.
The idea is, for a large number of possible events, there must be a quantity of entries valued to zero and this makes the matrix sparse.
Using the sum property described before we can take the old Yale format and adapt it, making the vector with row indices useless.

This script works with matrices represented as a list of lists, where every sublist is a row.